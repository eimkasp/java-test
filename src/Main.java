import java.text.DecimalFormat;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        int[] a = {87, 6, 11, 15, 8, 4};

        //
        int[] b = {87, 6, 11, 15, 8, 4};

        int suma = 0;
        double vidurkis;

        /* skaiciuojame masyvo elementu suma*/
        for(int i = 0; i < a.length; i++) {
            suma += a[i];
        }


        /* skaiciuojame vidurki */
        vidurkis = (double)suma / (double)a.length;

        /* apvaliname gauta rezultata */
        DecimalFormat df = new DecimalFormat("#.000");
        String rez = df.format(vidurkis);

        System.out.println(rez);
    }
}